import traceback

class dotable():
    def __init__(self,data):
        self.__edited__ = set()
        for key, value in data.items():
            self.set(key, value)

    def set(self, name, value):
        self.__edited__.add(name)
        setattr(self, name, value)
    
    def __dict__(self):
        out = {}
        for name in self.__edited__:
            out[name] = getattr(self, name)
        return out

    def __str__(self):
        return str(self.__dict__())

class key():
    def __init__(self, type_, values=None):
        self.type = type_
        self.data = dotable(values if type(values) == dict else {})
    
    def __str__(self):
        return f'<{self.type} :: {str(self.data)}>'

class errorExcept(Exception):
    def __init__(self, keyy, linenumber=0):
        super().__init__(f'[{linenumber}] {keyy.data.name}: {keyy.data.details}')

class AMLParser():
    def __init__(self, text='', inherit_variables=True):
        self.inherit_variables = inherit_variables
        self.setText(text)

    def setText(self, text):
        self.text = text
        self.reset()
    
    def reset(self):
        self.idx = 0
        self.lines = self.text.split('\n')
        self.vars = {'classes':{'root':self.get_class_prefab()}}
        self.vars['classes']['root']['root'] = True
        self.current_variable = self.vars['classes']['root']
        self.current_class = self.vars['classes']['root']
        self.current_descriptor = self.vars['classes']['root']
    
    def get_class(self, ancestor_sequence):
        current = self.vars['classes']['root']
        if len(ancestor_sequence) > 0 and ancestor_sequence[0][0] == '^':
            good = True
            for letter in ancestor_sequence[0]:
                if letter != '^':
                    good = False
            if good:
                ancestors = self.current_class['ancestors']+[self.current_class['name']]
                cut = len(ancestors)-len(ancestor_sequence[0])+1
                ancestors = ancestors[:cut]
                ancestor_sequence = ancestors + ancestor_sequence[1:]

        for i,child in enumerate(ancestor_sequence):
            me = current['children'].get(child, self.get_class_prefab())
            me['name'] = child
            me['ancestors'] = ancestor_sequence[:i]
            current['children'][child] = me
            current = me
        return current, ancestor_sequence
    
    def get_class_prefab(self):
        return {
            'ancestors': [],
            'description': [],
            'variables': {},
            'children': {},
            'name': '',
        }
    def get_variable_prefab(self):
        return {
            'description':[],
            'cases':[],
            'value':None,
            'name':''
        }
    
    def interpret(self):
        self.reset()

        endidx = len(self.lines)
        while self.idx < endidx:
            try:
                self.interpret_line()
            except Exception as e:
                print(f'Exception {e.__class__.__name__}: "{e}"')
                print(f'on line {self.idx+1}: "{self.lines[self.idx]}"')
                print()
                raise e
            self.idx += 1
        return self.vars['classes']['root']


    def interpret_line(self):
        self.cline = self.lines[self.idx].strip(' ')
        key = self.tokeniseLine(self.cline)
        
        if key.type == 'Empty':
            return

        elif key.type == 'Error':
            raise errorExcept(key)

        elif key.type == 'ClassStart':
            ancestors = key.data.ancestors
            c,ancestors = self.get_class(ancestors)
            me = self.get_class_prefab()
            c['children'][key.data.name] = me
            me['ancestors'] = ancestors
            if self.inherit_variables:
                me['variables'] = c['variables'].copy()
            me['name'] = key.data.name
            self.current_variable = me
            self.current_class = me
            self.current_descriptor = me

        elif key.type == 'Description':
            self.current_descriptor['description'].append(key.data.text.strip())
        
        elif key.type == 'KeyValuePair':
            tmp = self.get_variable_prefab()
            tmp['name'] = key.data.key
            val = key.data.value.strip()
            if val != '' and val[0] == '!':
                val,_ = self.get_class(val[1:].split(':'))
            tmp['value'] = val
            self.current_class['variables'][key.data.key] = tmp
            self.current_variable = tmp
            self.current_descriptor = tmp

        elif key.type == 'CaseActionPair':
            if self.current_variable.get('cases') != None or self.current_variable.get(''):
                tmp = {'case':key.data.case,'action':key.data.action, 'description':[]}
                self.current_variable['cases'].append(tmp)
                self.current_descriptor = tmp
            else:
                raise(KeyError(key('Error', values={'name':'SyntaxError', 'details':'A case > action pair can only be attached to a variable, not a class'})))
    
    def tokeniseLine(self, line):
        idx = 0
        buffer = ''
        L = len(line)

        while idx < L:
            char = line[idx]
            if char == '\\':
                buffer += line[idx+1]
                idx += 2
                continue
            elif char == '/':
                if idx+1<L and line[idx+1] == '/':
                    idx = L
                    continue
            if idx == 0:
                if char == '@':
                    c = line.count(':')
                    guys = line.split(':')
                    guys[0] = guys[0][1:]
                    return key('ClassStart',values={'ancestors': guys[:-1], 'name':guys[-1]})
                elif char == '#':
                    return key('Description',values={'text':line[1:]})
                elif char == ':':
                    return key('Error', values={'name':'SyntaxError', 'details':'key:value pair missing key'})
                else:
                    buffer += char
                    idx += 1
            else:
                if char == ':':
                    return key('KeyValuePair', values={'key':buffer,'value':line[idx+1:]})
                if char == '>':
                    return key('CaseActionPair', values={'case':buffer,'action':line[idx+1:]})
                else:
                    buffer += char
                    idx += 1
        return key('KeyValuePair', values={'key':buffer,'value':''})

def dict_to_aml(d):
    def escapify(seq):
        return seq.replace(':','\\:').replace('>','\\>')
    out = []
    ws = '    '

    if not d.get('root'):
        out.append(f'@{":".join(d["ancestors"]+[""])}{d["name"]}')
        for cmt in d["description"]:
            out.append(f'{ws}# {cmt}')
        for var in d['variables'].values():
            val = var['value']
            if type(val) == dict:
                val = f'!{":".join(val["ancestors"]+[""])}{val["name"]}'
                
            if val == '':
                out.append(f'{ws}{escapify(var["name"].strip())}')
            else:
                out.append(f'{ws}{escapify(var["name"].strip())}: {val.strip()}')
            for cmt in var["description"]:
                out.append(f'{ws}# {cmt.strip()}')

            for case in var['cases']:
                out.append(f'{ws}{ws}{escapify(case["case"].strip())} > {case["action"].strip()}')
                for cmt in case["description"]:
                    out.append(f'{ws}{ws}# {cmt.strip()}')
    for child in d['children'].values():
        out.append('')
        out.append(dict_to_aml(child))
    
    if d.get('root'):
        out = out[1:]
    return '\n'.join(out)



def main(filename):
    with open(filename, 'r') as f:
        b = f.read()
        a = AMLParser(b)
    res = a.interpret()
    aml = dict_to_aml(res)
    two = dict_to_aml(AMLParser(aml).interpret())
    with open('output.aml', 'w') as f:
        f.write(aml)
    
    
if __name__ == '__main__':
    main('standard.aml')